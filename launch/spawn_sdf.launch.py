import os
import random
from ament_index_python.packages import get_package_prefix
from launch.substitutions import LaunchConfiguration
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument


def generate_launch_description():

    DeclareLaunchArgument("name",    description="Vehicle entity name in Gazebo.")
    DeclareLaunchArgument("vehicle", description="Name of the model folder file " +
                                                 "in alpha_gazebo_vehicles/sdf.")


    pkg_path = get_package_prefix("alpha_gazebo_vehicles").replace("/install/", "/src/")

    vehicle_model_path = [pkg_path, "/sdf/", LaunchConfiguration("vehicle"), "/model.sdf"]



    return LaunchDescription([
        DeclareLaunchArgument("x", default_value="0.0"),
        DeclareLaunchArgument("y", default_value="0.0"),
        DeclareLaunchArgument("z", default_value="0.0"),
        DeclareLaunchArgument("p", default_value="0.0"),
        DeclareLaunchArgument("r", default_value="0.0"),
        DeclareLaunchArgument("Y", default_value="0.0"),
        Node(
            package="gazebo_ros",
            executable="spawn_entity.py",
            arguments=[ "-entity",
                    LaunchConfiguration("name"),
                    '-x', LaunchConfiguration('x'),
                    '-y', LaunchConfiguration('y'),
                    '-z', LaunchConfiguration('z'),
                    '-R', LaunchConfiguration('r'),
                    '-P', LaunchConfiguration('p'),
                    '-Y', LaunchConfiguration('Y'),
                     "-file",
                    vehicle_model_path
                    ],
            output="screen")
        ]
    )
            

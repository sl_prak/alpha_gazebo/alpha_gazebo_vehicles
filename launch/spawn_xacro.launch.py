import os
import xacro

from launch_ros.actions import Node
from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration
from launch.actions import OpaqueFunction, DeclareLaunchArgument
from ament_index_python.packages import get_package_share_directory


def robot_state_publisher_launch(context):
    namespace_str = str(LaunchConfiguration("model_ns").perform(context))

    xacro_dir_name = LaunchConfiguration('xacro_dir_name').perform(context)
    pkg_path = os.path.join(get_package_share_directory('alpha_gazebo_vehicles'))
    xacro_file_path = os.path.join(pkg_path,'urdf',xacro_dir_name,'model.xacro')
    xacro_file = xacro.process_file(xacro_file_path, mappings={'namespace' : namespace_str})
    
    params = {'robot_description': xacro_file.toxml(), 'use_sim_time': LaunchConfiguration('use_sim_time')}
    node_robot_state_publisher = Node(
        package='robot_state_publisher',
        namespace=LaunchConfiguration('model_ns'),
        executable='robot_state_publisher',
        output='log',
        parameters=[params]
    )

    return [node_robot_state_publisher]


def generate_launch_description():
    node_robot_state_publisher = OpaqueFunction(function=robot_state_publisher_launch)
    spawn_entity = Node(package='gazebo_ros', executable='spawn_entity.py',
                        namespace=LaunchConfiguration('model_ns'), 
                        arguments=['-topic', 'robot_description',
                                   '-entity', LaunchConfiguration('model_name'),
                                    '-x', LaunchConfiguration('x'),
                                    '-y', LaunchConfiguration('y'),
                                    '-z', LaunchConfiguration('z'),
                                    '-R', LaunchConfiguration('r'),
                                    '-P', LaunchConfiguration('p'),
                                    '-Y', LaunchConfiguration('Y')],
                        output='log')
    
    return LaunchDescription([
        DeclareLaunchArgument(
            "use_sim_time",
            default_value="true",
            description="Use sim time if true."),

        DeclareLaunchArgument(
            "verbose", 
            default_value="true"),

        DeclareLaunchArgument(
            "xacro_dir_name", 
            default_value="actros", 
            description="Xacro dir name in " + 
                        "alpha_ros_gazebo_vehicle/urdf."),

        DeclareLaunchArgument(
            "model_name", 
            default_value="car101", 
            description="Model name in gazebo."),                    

        DeclareLaunchArgument(
            "model_ns",
            default_value=""),
            
        DeclareLaunchArgument("x", default_value="0.0"),
        DeclareLaunchArgument("y", default_value="0.0"),
        DeclareLaunchArgument("z", default_value="0.0"),
        DeclareLaunchArgument("p", default_value="0.0"),
        DeclareLaunchArgument("r", default_value="0.0"),
        DeclareLaunchArgument("Y", default_value="0.0"),

        node_robot_state_publisher,

        spawn_entity
    ])







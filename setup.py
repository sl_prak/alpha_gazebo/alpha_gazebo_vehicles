from glob import glob
from setuptools import setup

PACKAGE_NAME = 'alpha_gazebo_vehicles'

setup(
    name=PACKAGE_NAME,
    version='0.1.0',
    packages=[PACKAGE_NAME],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + PACKAGE_NAME]),
        ('share/' + PACKAGE_NAME, ['package.xml']),
        ('share/' + PACKAGE_NAME + '/launch', glob('launch/*.launch.py')),
        ('share/' + PACKAGE_NAME + '/urdf/actros', glob('urdf/actros/*.xacro')),
        ('share/' + PACKAGE_NAME + '/urdf/actros/mesh', glob('urdf/actros/mesh/*')),
        ('lib/' + PACKAGE_NAME, glob('launch/*.py')),

    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Ivan Shevtsov',
    maintainer_email='ishevtsov0108@gmail.com',
    description='Package with a launch .urdf, .xacro and .sdf files in Gazebo simulator',
    license='BSD, Apache 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)

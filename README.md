# alpha_gazebo_vehicles

ROS2 пакет с моделями и `*.launch.py` файлами для запуска их в симуляторе gazebo.

## Установка вне контейнера

В директории workspace/src выполните:

```shell
git clone https://gitlab.com/sl_prak/alpha_gazebo_vehicles.git
cd ..
rosdep install --from-paths src --ignore-src -r -y
colcon build
```

При использовании [контейнера](https://gitlab.com/sl_prak/alpha_vehicle_simulator) нужно только склонировать репозиторий и собрать проект после сборки docker образа ([подробнее о сборке в контейнере](https://gitlab.com/sl_prak/alpha_vehicle_simulator/-/blob/develop/README.md)).

## Работа с пакетом

Запустите Gazebo: вы можете открыть свой мир или воспользоваться командой для запуска пустой симуляции:

```shell
ros2 launch gazebo_ros gazebo.launch.py 
```

### .sdf .urdf модели

Переместите папку с вашей моделью в дирикторию `<path_to_alpha_gazebo_vehicles>/sdf`. Файл модели должен быть назван `model.sdf`. Запустите лаунч файл, передав в качестве аргумента `name` имя модели, которое будет отображаться в симуляторе, а в качестве аргумента `vechicle` название дириктории с вашей моделью. В качестве примера в пакете содержится модель `car_008`, для её запуска в  необходимо выполнить команду:

```shell
ros2 launch alpha_gazebo_vehicles spawn_sdf.launch.py name:='car' vehicle:='car_008' x:='10.0' y:='40.0'
```

Указание координат и углов Эйлера являся опциональным, без них модель появится в начале координат с нулевым углом поворота относительно осей.

### .xacro модели

Для запуска xacro файлов нужна преварительная обработка, поэтому способ запуска будет отличаться. Поместите папку со своей модели по пути `<path_to_alpha_gazebo_vehicles>/urdf` и назовите основной исполняемый файл `model.xacro`. Имя папки с моделью нужо будет передать лаунч файлу при запуске в качестве аргумента `xacro_dir_name`. Также необходимо добавить путь до вашей модели в [setup.py](setup.py) файл проекта по приведённому там примеру модели actros.

Для запуска вашей модели выполните:

```shell
ros2 launch alpha_gazebo_vehicles spawn_xacro.launch.py xacro_dir_name:='actros'   model_name:='model_name_in_gazebo' x:='10.0' y:='30.0'
```

При запуске лаунч файла без параметров в симуляторе появится пример по умолчанию(actros) из репозитория. Параметры координат и углов Эйлера также являются опциональными.
